#	Gerenciador de Coleções Pessoais (GCP)

O projeto consiste em ser um gerenciador de coleções pessoais, com vários tipos de itens, sejam eles, jogos de video games, jogos de tabuleiro, quadrinhos, entre outros. O usuário tem a possibilidade de criar itens, editar, excluir e listar os mesmos. Ou seja, tem como objeto auxiliar no gerenciamento.

## Tutorial

Seguindo os passos abaixo, você terá um resultado satisfatório do projeto, possibilitando assim sua execução.


## Pré requisitos 

O que você precisará instalar é:

** Java JDK 1.8.0_121 **

** Java JRE 1.8.0_201 **


## Instalação 

## Para inicializar o projeto no seu computador, siga os passos:

** 1º Passo: **

Baixe este projeto em formato .zip. 
Descompacte o projeto para um local de sua preferência

** 2º Passo: **

Na pasta descompactada "ProjetoWeb", exclua o arquivo ".project" 

** 3° Passo: **

No eclipse, importe o projeto 

** File > Import > Maven > Existing Maven Projects > Browser > Seleciona o arquivo que você extraiu > Selecionar Pasta > Finish **

## Execução da aplicação

Após importar o projeto, automaticamente o Maven irá fazer o downloads das dependências necessárias (arquivo pom.xml) isso pode demorar um pouco, mas você pode acompanhar o andamento desses downloads no eclipse (Barra esquerda inferior "Progress"). 


## Notas

Local que se encontram os enums “Console”,”Estado”,”Localização”: 
** src/main/java/colecoes/enums **



Local que  se encontram  classes de itens “Dlcs”, “Item”, “JogosTabuleiro”, “JogosVideogame”, “Midia”, “Quadrinho”: 
** src/main/java/colecoes/itens **



Local que se encontram classes: “JogosTabuleiroDAO”, “JogosTabuleiroDAOWithLogs”, “JogosVideogameDAO”, “MidiaDAO”, “QuadrinhoDAO”,“JogosVideogameDAOWithLogs”, “MidiaDAOWithLogs”, “QuadrinhoDAOWithLogs”: 
** src/main/java/colecoes/registro **	



Local que se encontram as classes: “CreateException”, “DeleteException”, “UpdateException”: 
** src/main/java/excecoes **	



Local que se encontra a interface: “Generics”: 
** src/main/java/interface_generica **
Local que se encontram as classes: “Amigo”, ”Usuario”: 
** src/main/java/usuario **



Local que se encontram as classes: “AmigoDAO”, “AmigoDAOWithLogs”, “UsuarioDAO”, “UsuarioDAOWithLogs”: 
** src/main/java/usuario/registro **



Local que  se encontram as classes “JogosTabuleiroTest”, “JogosVideogame”,”MidiaTest”, “QuadrinhoTest”:
** src/test/java/testes/itens **



Local que se encontram as classes “AmigoTest”, “UsuarioTest”:
** src/test/java/testes/usuario **


Local que se encontra o "JavaDocs":
** /doc **


# Autores

### Adriele Barbosa da Costa
### Samara Lima Gouveia
### Tamyres Carla dos Santos

##Diagrama de Classes:
https://drive.google.com/open?id=10rMOetwvz2f3as9gXfvVWbMIi-rPujZ9