package excecoes;

/**
 * Classe UpdateException para atualizar os dados do registro
 * @author Adriele, Samara e Tamyres
 */
@SuppressWarnings("serial")
public class UpdateException extends Exception{
	
	/**
	 * Construtor que passa uma mensagem caso a atualizacao dos dados de errado
	 * @param mensagem
	 */
	public UpdateException(String mensagem) {
		super(mensagem);
	}

}
