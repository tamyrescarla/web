package excecoes;

/**
 * Classe CreateException para a criacao do registro
 * @author Adriele, Samara e Tamyres
 */
@SuppressWarnings("serial")
public class CreateException extends Exception {
	
	/**
	 * Construtor que passa uma mensagem caso a criacao do registro tenha dado errado
	 * @param mensagem
	 */
	public CreateException(String mensagem) {
		super(mensagem);
	}

}
