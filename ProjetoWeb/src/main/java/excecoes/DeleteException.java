package excecoes;

/**
 * Classe DeleteException para excluir o registro
 * @author Adriele, Samara e Tamyres
 */
@SuppressWarnings("serial")
public class DeleteException extends Exception{

	/**
	 * Construtor que passa uma mensagem quando ao tentar excluir um registro algo de errado
	 * @param mensagem
	 */
	public DeleteException(String mensagem) {
		super(mensagem);
	}
}
