package usuario;

/**
 * Classe Amigo
 * @author Adriele, Samara e Tamyes
 */
public class Amigo {

	private int idAmigo;
	private String nome;
	private String sexo;
	private String parentesco;
	private String endereco;
	private String telefone;

	/**
	 * Construtor
	 * @param idAmigo
	 * @param nome
	 * @param sexo
	 * @param parentesco
	 * @param endereco
	 * @param telefone
	 */
	public Amigo(int idAmigo, String nome, String sexo, String parentesco, String endereco, String telefone) {
		this.idAmigo = idAmigo;
		this.nome = nome;
		this.sexo = sexo;
		this.parentesco = parentesco;
		this.endereco = endereco;
		this.telefone = telefone;
	}
	
	/**
	 * Retorna o id
	 * @return
	 */
	public int getIdAmigo() {
		return idAmigo;
	}

	/**
	 * Seta o id
	 * @param idAmigo
	 */
	public void setIdAmigo(int idAmigo) {
		this.idAmigo = idAmigo;
	}
	
	/**
	 * Retorna o nome
	 * @return
	 */
	public String getNome() {
		return nome;
	}

	/**
	 * Seta o nome
	 * @param nome
	 */
	public void setNome(String nome) {
		this.nome = nome;
	}

	/**
	 * Retorna o sexo
	 * @return
	 */
	public String getSexo() {
		return sexo;
	}

	/**
	 * Seta o sexo
	 * @param sexo
	 */
	public void setSexo(String sexo) {
		this.sexo = sexo;
	}

	/**
	 * Retorna o parentesco
	 * @return
	 */
	public String getParentesco() {
		return parentesco;
	}

	/**
	 * Seta o parentesco
	 * @param parentesco
	 */
	public void setParentesco(String parentesco) {
		this.parentesco = parentesco;
	}

	/**
	 * Retorna o endereco
	 * @return
	 */
	public String getEndereco() {
		return endereco;
	}

	/**
	 * Seta o endereco
	 * @param endereco
	 */
	public void setEndereco(String endereco) {
		this.endereco = endereco;
	}

	/**
	 * Retorna o telefone
	 * @return
	 */
	public String getTelefone() {
		return telefone;
	}

	/**
	 * Seta o telefone
	 * @param telefone
	 */
	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}

	@Override
	public String toString() {
		return "Amigo [idAmigo=" + idAmigo + ", nome=" + nome + ", sexo=" + sexo + ", parentesco=" + parentesco
				+ ", endereco=" + endereco + ", telefone=" + telefone + "]";
	}

}
