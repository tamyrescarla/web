package usuario;

/**
 * Classe Usuario
 * @author Adriele, Samara e Tamyres
 */
public class Usuario {
	
	private int id;
	private String login;
	private String senha;
	private String nome;
	
	/**
	 * Construtor
	 * @param id
	 * @param login
	 * @param senha
	 * @param nome
	 */
	public Usuario(int id, String login, String senha, String nome) {
		this.id = id;
		this.login = login;
		this.senha = senha;
		this.nome = nome;
	}

	/**
	 * Retorna o id
	 * @return
	 */
	public int getId() {
		return id;
	}

	/**
	 * Seta o id
	 * @param id
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * Retorna o login
	 * @return
	 */
	public String getLogin() {
		return login;
	}

	/**
	 * Seta o login
	 * @param login
	 */
	public void setLogin(String login) {
		this.login = login;
	}

	/**
	 * Retorna a senha
	 * @return
	 */
	public String getSenha() {
		return senha;
	}

	/**
	 * Seta a senha
	 * @param senha
	 */
	public void setSenha(String senha) {
		this.senha = senha;
	}

	/**
	 * Retorna o nome
	 * @return
	 */
	public String getNome() {
		return nome;
	}

	/**
	 * Seta o nome
	 * @param nome
	 */
	public void setNome(String nome) {
		this.nome = nome;
	}

	@Override
	public String toString() {
		return "Usuario [id=" + id + ", login=" + login + ", senha=" + senha + ", nome=" + nome + "]";
	}
	
}
