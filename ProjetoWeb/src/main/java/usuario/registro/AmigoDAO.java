package usuario.registro;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import excecoes.CreateException;
import excecoes.DeleteException;
import excecoes.UpdateException;
import interface_generica.Generics;
import usuario.Amigo;

/**
 * Classe AmigoDAO que implementa a interface Generics
 * @author Adriele, Samara e Tamyres
 */
public class AmigoDAO implements Generics<Amigo> {
	
	private List<Amigo> amigos;
	
	Scanner in = new Scanner(System.in);

	/**
	 * Construtor 
	 */
	public AmigoDAO() {
		amigos = new ArrayList<Amigo>();
	}

	@Override
	public boolean create(Amigo amigo) throws CreateException {
		if (!amigos.contains(amigo)) {
			amigos.add(amigo);
			return true;
		} else throw new CreateException("O amigo ja esta registrado");
		
	}

	@Override
	public boolean update(Amigo amigo) throws UpdateException {
		if (amigos.contains(amigo)) {
			System.out.println("----------ATUALIZAR REGISTRO DO AMIGO----------");
			amigo.setIdAmigo(in.nextInt());
			amigo.setNome(in.nextLine());
			amigo.setSexo(in.nextLine());
			amigo.setParentesco(in.nextLine());
			amigo.setEndereco(in.nextLine());
			amigo.setTelefone(in.nextLine());
			amigos.add(amigo);
			return true;
		} else throw new UpdateException("O amigo nao esta registrado");
	}

	@Override
	public boolean delete(Amigo amigo) throws DeleteException {
		if (amigos.contains(amigo)) {
			amigos.remove(amigo);
			return true;
		} else throw new DeleteException("O amigo nao esta registrado");
	}

		
	@Override
	public List<Amigo> listar() {
		return amigos;
	}
}
