package usuario.registro;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import excecoes.CreateException;
import excecoes.DeleteException;
import excecoes.UpdateException;
import interface_generica.Generics;
import usuario.Usuario;

/**
 * Classe UsuarioDAO que implementa a interface Generics
 * @author Adriele, Samara e Tamyres
 */
public class UsuarioDAO implements Generics<Usuario> {
	
	private List<Usuario> usuarios;
	
	Scanner in = new Scanner(System.in);

	/**
	 * Construtor 
	 */
	public UsuarioDAO() {
		usuarios = new ArrayList<Usuario>();
	}

	@Override
	public boolean create(Usuario usuario) throws CreateException {
		if (!usuarios.contains(usuario)) {
			usuarios.add(usuario);
			return true;
		} else throw new CreateException("O usuario ja esta registrado");
		
	}

	@Override
	public boolean update(Usuario usuario) throws UpdateException {
		if (usuarios.contains(usuario)) {
			System.out.println("----------ATUALIZAR REGISTRO DO USUARIO----------");
			usuario.setLogin(in.nextLine());
			usuario.setSenha(in.nextLine());
			usuario.setNome(in.nextLine());
			usuarios.add(usuario);
			return true;
		} else throw new UpdateException("O usuario nao esta registrado");
	}

	@Override
	public boolean delete(Usuario usuario) throws DeleteException {
		if (usuarios.contains(usuario)) {
			usuarios.remove(usuario);
			return true;
		} else throw new DeleteException("O usuario nao esta registrado");
	}

		
	@Override
	public List<Usuario> listar() {
		return usuarios;
	}

}
