package usuario.registro;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import excecoes.CreateException;
import excecoes.DeleteException;
import excecoes.UpdateException;
import usuario.Usuario;

/**
 * Classe UsuarioDAOWithLogs
 * @author Adriele, Samara, Tamyres
 */
public class UsuarioDAOWithLogs {

	private List<Usuario> usuarios;
	Scanner in = new Scanner(System.in);
	private static Logger logger = LoggerFactory.getLogger(UsuarioDAOWithLogs.class);

	/**
	 * Construtor
	 */
	public UsuarioDAOWithLogs() {
		usuarios = new ArrayList<Usuario>();
	}

	/**
	 * Metodo para criar um usuario
	 * @param usuario
	 * @return true para usuario criado
	 * @throws CreateException
	 */
	public boolean create(Usuario usuario) throws CreateException {
		logger.info("Executando o metodo create", usuario);
		if (!usuarios.contains(usuario)) {
			usuarios.add(usuario);
			logger.info("O usuario foi registrado: {}", true);
			return true;
		} else
			throw new CreateException("O usuário ja esta registrado");

	}

	/**
	 * Metodo para atualizar um usuario
	 * @param usuario
	 * @return true para usuario atualizado
	 * @throws UpdateException
	 */
	public boolean update(Usuario usuario) throws UpdateException {
		logger.info("Executando o metodo update", usuario);
		if (usuarios.contains(usuario)) {
			System.out.println("----------ATUALIZAR REGISTRO DO USUARIO----------");
			usuario.setLogin(in.nextLine());
			usuario.setSenha(in.nextLine());
			usuario.setNome(in.nextLine());
			usuarios.add(usuario);
			logger.info("O registro do usuario foi atualizado", true);
			return true;
		} else
			throw new UpdateException("O usuario nao esta registrado");
	}

	/**
	 * Metodo para deletar um usuario
	 * @param usuario
	 * @return true para usuario removido
	 * @throws DeleteException
	 */
	public boolean delete(Usuario usuario) throws DeleteException {
		logger.info("Executando o metodo delete", usuario);
		if (usuarios.contains(usuario)) {
			usuarios.remove(usuario);
			logger.info("O usuario foi removido", true);
			return true;
		} else
			throw new DeleteException("O usuario nao esta registrado");
	}

	/**
	 * Metodo para listar usuarios
	 * @return List
	 */
	public List<Usuario> listar() {
		return usuarios;
	}

	public static void main(String[] args) {

		UsuarioDAOWithLogs cr = new UsuarioDAOWithLogs();
		Usuario usuario = new Usuario(1, "tam", "123", "Tamyres");

		try {

			cr.create(usuario);
			cr.create(usuario);

		} catch (Exception e) {
			logger.error(e.getMessage());
			logger.error("Erro ao inserir usuario", e);
		}

		try {
			cr.update(usuario);

		} catch (Exception a) {
			logger.error(a.getMessage());
			logger.error("Erro ao atualizar usuario", a);

		}

		try {
			cr.delete(usuario);

		} catch (Exception b) {
			logger.error(b.getMessage());
			logger.error("Erro ao remover usuario", b);

		}

	}

}
