package usuario.registro;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import excecoes.CreateException;
import excecoes.DeleteException;
import excecoes.UpdateException;
import usuario.Amigo;
import usuario.Usuario;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class AmigoDAOWithLogs  {

	private List<Amigo> amigos;
	Scanner in = new Scanner(System.in);
	private static Logger logger = LoggerFactory.getLogger(UsuarioDAOWithLogs.class);

	/**
	 * Construtor
	 */
	public AmigoDAOWithLogs() {
		amigos = new ArrayList<Amigo>();
	}

	public boolean create(Amigo amigo) throws CreateException {
		logger.info("Executando o metodo create", amigo);
		if (!amigos.contains(amigo)) {
			amigos.add(amigo);
			logger.info("O amigo foi registrado: {}", true);
			return true;
		} else
			throw new CreateException("O amigo ja esta registrado");

	}

	public boolean update(Amigo amigo) throws UpdateException {
		logger.info("Executando o metodo update", amigo);
		if (amigos.contains(amigo)) {
			System.out.println("----------ATUALIZAR REGISTRO DO AMIGO----------");
			amigo.setNome(in.nextLine());
			amigo.setSexo(in.nextLine());
			amigo.setParentesco(in.nextLine());
			amigo.setEndereco(in.nextLine());
			amigo.setTelefone(in.nextLine());
			amigos.add(amigo);
			logger.info("O amigo foi atualizado: {}", true);
			return true;
		} else
			throw new UpdateException("O amigo não esta registrado");
	}

	public boolean delete(Amigo amigo) throws DeleteException {
		logger.info("Executando o metodo delete", amigo);
		if (amigos.contains(amigo)) {
			amigos.remove(amigo);
			logger.info("O amigo foi removido: {}", true);
			return true;
		} else
			throw new DeleteException("O amigo nao esta registrado");
	}

	public List<Amigo> listar() {
		return amigos;
	}

	public static void main(String[] args) {

		AmigoDAOWithLogs cr = new AmigoDAOWithLogs();
		Amigo amigo = new Amigo(1, "tamyres", "feminino", "prima", "rua linda", "88888");

		try {

			cr.create(amigo);
			cr.create(amigo);

		} catch (Exception e) {
			logger.error(e.getMessage());
			logger.error("Erro ao inserir amigo", e);
		}

		try {
			cr.update(amigo);

		} catch (Exception a) {
			logger.error(a.getMessage());
			logger.error("Erro ao atualizar amigo", a);

		}

		try {
			cr.delete(amigo);

		} catch (Exception b) {
			logger.error(b.getMessage());
			logger.error("Erro ao remover amigo", b);

		}
	}
}
