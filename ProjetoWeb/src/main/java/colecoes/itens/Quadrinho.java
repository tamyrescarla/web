package colecoes.itens;

import colecoes.enums.Estado;

/**
 * Classe Quadrinho que herda atributos da classe Item
 * @author Adriele, Samara e Tamyres
 */
public class Quadrinho extends Item {
	
	private int numero;
	private String editora;
	private String universo;
	private String saga;
	private boolean statusLido;
	
	/**
	 * Construtor
	 * @param id
	 * @param titulo
	 * @param estado
	 * @param preco
	 * @param observacoes
	 * @param importante
	 * @param numero
	 * @param editora
	 * @param universo
	 * @param saga
	 * @param statusLido
	 */
	public Quadrinho(int id, String titulo, Estado estado, double preco, String observacoes, boolean importante, int numero, String editora, String universo, String saga, boolean statusLido) {
		super(id, titulo, estado, preco, observacoes, importante);
		this.numero = numero;
		this.editora = editora;
		this.universo = universo;
		this.saga = saga;
		this.statusLido = statusLido;
	}

	/**
	 * Retorna o numero
	 * @return
	 */
	public int getNumero() {
		return numero;
	}

	/**
	 * Seta o numero
	 * @param numero
	 */
	public void setNumero(int numero) {
		this.numero = numero;
	}

	/**
	 * Retorna a editora
	 * @return
	 */
	public String getEditora() {
		return editora;
	}

	/**
	 * Seta a editora
	 * @param editora
	 */
	public void setEditora(String editora) {
		this.editora = editora;
	}

	/**
	 * Retorna o universo
	 * @return
	 */
	public String getUniverso() {
		return universo;
	}

	/**
	 * Seta o universo
	 * @param universo
	 */
	public void setUniverso(String universo) {
		this.universo = universo;
	}

	/**
	 * Retorna a saga
	 * @return
	 */
	public String getSaga() {
		return saga;
	}

	/**
	 * Seta a saga
	 * @param saga
	 */
	public void setSaga(String saga) {
		this.saga = saga;
	}

	/**
	 * Retorna o status
	 * @return
	 */
	public boolean getStatusLido() {
		return statusLido;
	}

	/**
	 * Seta o status
	 * @param statusLido
	 */
	public void setStatusLido(boolean statusLido) {
		this.statusLido = statusLido;
	}

	@Override
	public String toString() {
		return "Quadrinho [numero=" + numero + ", editora=" + editora + ", universo=" + universo + ", saga=" + saga
				+ ", statusLido=" + statusLido + "]";
	}
	
}
