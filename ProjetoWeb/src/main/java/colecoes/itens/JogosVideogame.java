package colecoes.itens;

import java.util.ArrayList;
import java.util.List;

import colecoes.enums.Console;
import colecoes.enums.Estado;

/**
 * Classe JogosVideogame que herda atributos da classe Item
 * 
 * @author Adriele, Samara e Tamyres
 */
public class JogosVideogame extends Item {

	private Console console;
	private boolean statusZerado;
	private boolean statusDlcs;
	private List<Dlcs> dlcs;

	/**
	 * Construtor
	 * 
	 * @param titulo
	 * @param estado
	 * @param preco
	 * @param observacoes
	 * @param importante
	 * @param console
	 * @param statusZerado
	 * @param statusDlcs
	 */
	public JogosVideogame(int id, String titulo, Estado estado, double preco, String observacoes, boolean importante,
			Console console, boolean statusZerado, boolean statusDlcs) {
		super(id, titulo, estado, preco, observacoes, importante);
		this.console = console;
		this.statusZerado = statusZerado;
		this.statusDlcs = statusDlcs;
		dlcs = new ArrayList<Dlcs>();
	}

	/**
	 * Retorna o console
	 * 
	 * @return
	 */
	public Console getConsole() {
		return console;
	}

	/**
	 * Seta o console
	 * 
	 * @param console
	 */
	public void setConsole(Console console) {
		this.console = console;
	}

	/**
	 * Retorna o statusZerado
	 * 
	 * @return
	 */
	public boolean getStatusZerado() {
		return statusZerado;
	}

	/**
	 * Seta o statusZerado
	 * 
	 * @param statusZerado
	 */
	public void setStatusZerado(boolean statusZerado) {
		this.statusZerado = statusZerado;
	}

	/**
	 * Retorna o statusDlcs
	 * 
	 * @return
	 */
	public boolean getStatusDlcs() {
		return statusDlcs;
	}

	/**
	 * Seta o statusDlcs
	 * 
	 * @param statusDlcs
	 */
	public void setStatusDlcs(boolean statusDlcs) {
		this.statusDlcs = statusDlcs;
	}

	/**
	 * Retorna a lista de Dlcs
	 * 
	 * @return
	 */
	public List<Dlcs> getDlcs() {
		return dlcs;
	}

	/**
	 * Seta a lista de Dlcs
	 * 
	 * @param dlcs
	 */
	public void setDlcs(List<Dlcs> dlcs) {
		this.dlcs = dlcs;
	}

	/**
	 * Imprime os dados
	 * 
	 * @return
	 */
	public String imprimir() {
		StringBuilder sb = new StringBuilder();
		sb.append("Console: " + getConsole().name() + "\nZerado: " + getStatusZerado() + "\nDLC's: " + getDlcs());
		return sb.toString();
	}

	@Override
	public String toString() {
		String dlc = "";
		if (getStatusDlcs()) {
			dlc = "DLC's: " + getDlcs().toString();
		}
		return super.toString() + imprimir();
	}
}