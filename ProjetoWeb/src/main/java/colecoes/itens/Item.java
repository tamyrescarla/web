package colecoes.itens;

import colecoes.enums.Estado;

/**
 * Classe Item
 * @author Adriele, Samara e Tamyres
 */
public class Item {

	protected int id;
	protected String titulo;
	protected Estado estado;
	protected double preco;
	protected String observacoes;
	protected boolean importante;
	
	/**
	 * Construtor
	 * @param titulo
	 * @param estado
	 * @param preco
	 * @param observacoes
	 * @param importante
	 */
	public Item(int id, String titulo, Estado estado, double preco, String observacoes, boolean importante) {
		this.id = id;
		this.titulo = titulo;
		this.estado = estado;
		this.preco = preco;
		this.observacoes = observacoes;
		this.importante = importante;
	}
	
	/**
	 * Retorna o id
	 * @return
	 */
	public int getId() {
		return id;
	}

	/**
	 * Seta o id
	 * @param id
	 */
	public void setId(int id) {
		this.id = id;
	}
	
	/**
	 * Retorna o titulo
	 * @return
	 */
	public String getTitulo() {
		return titulo;
	}

	/**
	 * Seta o titulo
	 * @param titulo
	 */
	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	/**
	 * Retorna o estado
	 * @return
	 */
	public Estado getEstado() {
		return estado;
	}

	/**
	 * Seta o estado
	 * @param estado
	 */
	public void setEstado(Estado estado) {
		this.estado = estado;
	}

	/**
	 * Retorna o preco
	 * @return
	 */
	public double getPreco() {
		return preco;
	}

	/**
	 * Seta o preco
	 * @param preco
	 */
	public void setPreco(double preco) {
		this.preco = preco;
	}

	/**
	 * Retorna as observacoes
	 * @return
	 */
	public String getObservacoes() {
		return observacoes;
	}

	/**
	 * Seta as observacoes
	 * @param observacoes
	 */
	public void setObservacoes(String observacoes) {
		this.observacoes = observacoes;
	}

	/**
	 * Retorna a importancia
	 * @return
	 */
	public boolean getImportante() {
		return importante;
	}

	/**
	 * Seta a importancia
	 * @param importante
	 */
	public void setImportante(boolean importante) {
		this.importante = importante;
	}

	@Override
	public String toString() {
		return "Item [id=" + id + ", titulo=" + titulo + ", estado=" + estado + ", preco=" + preco + ", observacoes="
				+ observacoes + ", importante=" + importante + "]";
	}


}
