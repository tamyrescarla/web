package colecoes.itens;

import colecoes.enums.Estado;

/**
 * Classe Midia que herda atributos da classe Item
 * @author Adriele, Samara e Tamyres
 */
public class Midia extends Item {
	
	private String marca;
	private String conteudo;
	private boolean statusAssistido;
	
	/**
	 * Construtor
	 * @param titulo
	 * @param estado
	 * @param preco
	 * @param observacoes
	 * @param importante
	 * @param marca
	 * @param conteudo
	 * @param statusAssistido
	 */
	public Midia(int id, String titulo, Estado estado, double preco, String observacoes, boolean importante, String marca, String conteudo, boolean statusAssistido) {
		super(id, titulo, estado, preco, observacoes, importante);
		this.marca = marca;
		this.conteudo = conteudo;
		this.statusAssistido = statusAssistido;
	}

	/**
	 * Retorna a marca
	 * @return
	 */
	public String getMarca() {
		return marca;
	}

	/**
	 * Seta a marca
	 * @param marca
	 */
	public void setMarca(String marca) {
		this.marca = marca;
	}

	/**
	 * Retorna o conteudo
	 * @return
	 */
	public String getConteudo() {
		return conteudo;
	}

	/**
	 * Seta o conteudo
	 * @param conteudo
	 */
	public void setConteudo(String conteudo) {
		this.conteudo = conteudo;
	}

	/**
	 * Retorna o status
	 * @return
	 */
	public boolean getStatusAssistido() {
		return statusAssistido;
	}

	/**
	 * Seta o status
	 * @param statusAssistido
	 */
	public void setStatusAssistido(boolean statusAssistido) {
		this.statusAssistido = statusAssistido;
	}

	@Override
	public String toString() {
		return "Midia [marca=" + marca + ", conteudo=" + conteudo + ", statusAssistido=" + statusAssistido + "]";
	}

}
