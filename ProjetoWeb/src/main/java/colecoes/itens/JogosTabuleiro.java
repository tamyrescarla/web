package colecoes.itens;

import colecoes.enums.Estado;

/**
 * Classe JogosTabuleiro que herda atributos da classe Item
 * @author Adriele, Samara e Tamyres
 */
public class JogosTabuleiro extends Item {

	/**
	 * Construtor
	 * @param id
	 * @param titulo
	 * @param estado
	 * @param preco
	 * @param observacoes
	 * @param importante
	 */
	public JogosTabuleiro(int id, String titulo, Estado estado, double preco, String observacoes, boolean importante) {
		super(id, titulo, estado, preco, observacoes, importante);
	}
	
}
