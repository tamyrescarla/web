package colecoes.itens;

import colecoes.enums.Localizacao;

/**
 * Classe Dlcs
 * @author Adriele, Samara e Tamyres
 */
public class Dlcs {
	
	private String titulo;
	private Localizacao localizacao;
	
	/**
	 * Construtor
	 * @param titulo
	 * @param localizacao
	 */
	public Dlcs(String titulo, Localizacao localizacao) {
		this.titulo = titulo;
		this.localizacao = localizacao;
	}
	
	/**
	 * Retorna o titulo
	 * @return
	 */
	public String getTitulo() {
		return titulo;
	}
	
	/**
	 * Seta o titulo
	 * @param titulo
	 */
	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}
	
	/**
	 * Retorna a localizacao
	 * @return
	 */
	public Localizacao getLocalizacao() {
		return localizacao;
	}
	
	/**
	 * Seta a localizacao
	 * @param localizacao
	 */
	public void setLocalizacao(Localizacao localizacao) {
		this.localizacao = localizacao;
	}
	
	@Override
	public String toString() {
		return "Dlcs [titulo=" + titulo + ", localizacao=" + localizacao + "]";
	}
}

