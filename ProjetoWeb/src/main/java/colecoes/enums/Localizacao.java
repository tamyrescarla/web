package colecoes.enums;

/**
 * Enum Localizacao
 * @author Adriele, Samara e Tamyres
 */
public enum Localizacao {
	
	LIVE("live"), PSN("psn"), STEAM("steam"), DISCO("disco");
	
	private String tipoLocalizacao;
	
	/**
	 * Construtor
	 * @param tipoLocalizacao
	 */
	Localizacao(String tipoLocalizacao) {
		this.setTipoLocalizacao(tipoLocalizacao);
	}

	/**
	 * Retorna a localizacao
	 * @return
	 */
	public String getTipoLocalizacao() {
		return tipoLocalizacao;
	}

	/**
	 * Seta a localizacao
	 * @param tipoLocalizacao
	 */
	public void setTipoLocalizacao(String tipoLocalizacao) {
		this.tipoLocalizacao = tipoLocalizacao;
	}

}
