package colecoes.enums;

/**
 * Enum Console
 * @author Adriele, Samara e Tamyres
 */
public enum Console {
	
	XBOXONE("xbox one"), PLAYSTATION("playstation"), NITENDO("nitendo"), OUTRO("outro");
	
	private String tipoConsole;
	
	/**
	 * Construtor
	 * @param tipoConsole
	 */
	Console(String tipoConsole) {
		this.setTipoConsole(tipoConsole);
	}

	/**
	 * Retorna o console
	 * @return
	 */
	public String getTipoConsole() {
		return tipoConsole;
	}

	/**
	 * Seta o console
	 * @param tipoConsole
	 */
	public void setTipoConsole(String tipoConsole) {
		this.tipoConsole = tipoConsole;
	}

}
