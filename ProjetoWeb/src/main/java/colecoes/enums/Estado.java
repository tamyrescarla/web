package colecoes.enums;

/**
 * Enum Estado
 * @author Adriele, Samara e Tamyres
 */
public enum Estado {
	
	NOVO("novo"), RASURADO("rasurado"), VELHO("velho"), OUTRO("outro");
	
	private String tipoEstado;
	
	/**
	 * Construtor
	 * @param tipoEstado
	 */
	Estado(String tipoEstado) {
		this.setTipoEstado(tipoEstado);
	}

	/**
	 * Retorna o estado
	 * @return
	 */
	public String getTipoEstado() {
		return tipoEstado;
	}

	/**
	 * Seta o estado
	 * @param tipoEstado
	 */
	public void setTipoEstado(String tipoEstado) {
		this.tipoEstado = tipoEstado;
	}

}
