package colecoes.registro;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import colecoes.enums.Estado;
import colecoes.itens.Item;
import colecoes.itens.JogosTabuleiro;
import excecoes.CreateException;
import excecoes.DeleteException;
import excecoes.UpdateException;
import usuario.Amigo;
import usuario.registro.AmigoDAOWithLogs;

/**
 * Classe JogosTabuleiroDAOWithLogs
 * 
 * @author Adriele, Samara, Tamyres
 */
public class JogosTabuleiroDAOWithLogs {

	private List<JogosTabuleiro> tabuleiro;

	Scanner in = new Scanner(System.in);
	private static Logger logger = LoggerFactory.getLogger(JogosTabuleiroDAOWithLogs.class);

	/**
	 * Construtor
	 */
	public JogosTabuleiroDAOWithLogs() {
		tabuleiro = new ArrayList<JogosTabuleiro>();
	}

	/**
	 * Metodo de criar um jogo de tabuleiro
	 * 
	 * @param item
	 * @return true para jogo registrado
	 * @throws CreateException
	 */
	public boolean create(JogosTabuleiro item) throws CreateException {
		logger.info("Executando o metodo create", item);
		if (!tabuleiro.contains(item)) {
			tabuleiro.add(item);
			logger.info("O jogo de tabuleiro foi registrado: {}", true);
			return true;
		} else
			throw new CreateException("O jogo de tabuleiro ja esta registrado");

	}

	/**
	 * Metodo de atualizar um jogo de tabuleiro
	 * 
	 * @param item
	 * @return true para jogo atualizado
	 * @throws UpdateException
	 */
	public boolean update(JogosTabuleiro item) throws UpdateException {
		if (tabuleiro.contains(item)) {
			logger.info("Executando o metodo update", item);
			System.out.println("----------ATUALIZAR REGISTRO DO JOGO DE TABULEIRO----------");
			item.setTitulo(in.nextLine());
			item.setEstado(Estado.valueOf(in.nextLine()));
			item.setPreco(in.nextDouble());
			item.setObservacoes(in.next());
			item.setImportante(in.nextBoolean());
			tabuleiro.add(item);
			logger.info("O jogo de tabuleiro foi atualizado: {}", true);
			return true;
		} else
			throw new UpdateException("O jogo de tabuleiro nao esta registrado");
	}

	/**
	 * Metodo de excluir um jogo de tabuleiro
	 * 
	 * @param item
	 * @return true para jogo removido
	 * @throws DeleteException
	 */
	public boolean delete(JogosTabuleiro item) throws DeleteException {
		logger.info("Executando o metodo delete", item);
		if (tabuleiro.contains(item)) {
			tabuleiro.remove(item);
			logger.info("O jogo de tabuleiro foi removido: {}", true);
			return true;
		} else
			throw new DeleteException("O jogo de tabuleiro nao esta registrado");
	}

	/**
	 * Metodo de listar jogos de tabuleiro
	 * 
	 * @return List
	 */
	public List<JogosTabuleiro> listar() {
		return tabuleiro;
	}

	/**
	 * Main para teste de criate, update e delete
	 * 
	 * @param args
	 */
	public static void main(String[] args) {

		JogosTabuleiroDAOWithLogs cr = new JogosTabuleiroDAOWithLogs();

		JogosTabuleiro item = new JogosTabuleiro(1, "damas", Estado.NOVO, 100.00, "legal", true);

		try {

			cr.create(item);
			cr.create(item);

		} catch (Exception e) {
			logger.error(e.getMessage());
			logger.error("Erro ao inserir jogo de tabuleiro", e);
		}

		try {
			cr.update(item);

		} catch (Exception a) {
			logger.error(a.getMessage());
			logger.error("Erro ao atualizar jogo de tabuleiro", a);

		}

		try {
			cr.delete(item);

		} catch (Exception b) {
			logger.error(b.getMessage());
			logger.error("Erro ao remover jogo de tabuleiro", b);

		}
	}

}
