package colecoes.registro;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import colecoes.enums.Console;
import colecoes.enums.Estado;
import colecoes.itens.JogosVideogame;
import excecoes.CreateException;
import excecoes.DeleteException;
import excecoes.UpdateException;
import interface_generica.Generics;

/**
 * Classe JogosVideogameDAO que implementa a interface Generics
 * 
 * @author Adriele, Samara e Tamyres
 */
public class JogosVideogameDAO implements Generics<JogosVideogame> {

	private List<JogosVideogame> videogame;

	Scanner in = new Scanner(System.in);

	/**
	 * Construtor
	 */
	public JogosVideogameDAO() {
		videogame = new ArrayList<JogosVideogame>();
	}

	@Override
	public boolean create(JogosVideogame item) throws CreateException {
		if (!videogame.contains(item)) {
			videogame.add(item);
			return true;
		} else
			throw new CreateException("O jogo de video game ja esta registrado");
	}

	@Override
	public boolean update(JogosVideogame item) throws UpdateException {
		if (videogame.contains(item)) {
			System.out.println("----------ATUALIZAR REGISTRO DO VIDEO GAME----------");
			item.setId(in.nextInt());
			item.setTitulo(in.nextLine());
			item.setEstado(Estado.valueOf(in.nextLine()));
			item.setPreco(in.nextDouble());
			item.setObservacoes(in.nextLine());
			item.setImportante(in.nextBoolean());
			item.setConsole(Console.valueOf(in.nextLine()));
			item.setStatusZerado(in.nextBoolean());
			item.setStatusDlcs(in.nextBoolean());
			videogame.add(item);
			return true;
		} else
			throw new UpdateException("O jogo de video game nao esta registrado");
	}

	@Override
	public boolean delete(JogosVideogame item) throws DeleteException {
		if (videogame.contains(item)) {
			videogame.remove(item);
			return true;
		} else
			throw new DeleteException("O jogo de video game nao esta registrado");
	}

	@Override
	public List<JogosVideogame> listar() {
		return videogame;
	}

}
