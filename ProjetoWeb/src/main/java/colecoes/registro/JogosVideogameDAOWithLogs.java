package colecoes.registro;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import colecoes.enums.Console;
import colecoes.enums.Estado;
import colecoes.itens.JogosTabuleiro;
import colecoes.itens.JogosVideogame;
import excecoes.CreateException;
import excecoes.DeleteException;
import excecoes.UpdateException;

/**
 * Classe JogosVideogameDAOWithLogs
 * 
 * @author Adriele, Samara, Tamyres
 */
public class JogosVideogameDAOWithLogs {

	private List<JogosVideogame> videogame;

	Scanner in = new Scanner(System.in);
	private static Logger logger = LoggerFactory.getLogger(JogosTabuleiroDAOWithLogs.class);

	/**
	 * Construtor
	 */
	public JogosVideogameDAOWithLogs() {
		videogame = new ArrayList<JogosVideogame>();
	}

	/**
	 * Metodo de criar um jogo de video game
	 * 
	 * @param item
	 * @return true para jogo registrado
	 * @throws CreateException
	 */
	public boolean create(JogosVideogame item) throws CreateException {
		logger.info("Executando o metodo create", item);
		if (!videogame.contains(item)) {
			videogame.add(item);
			logger.info("O jogo de video game foi registrado: {}", true);
			return true;
		} else
			throw new CreateException("O jogo de video game ja esta registrado");
	}

	/**
	 * Metodo de atualizar um jogo de video game
	 * 
	 * @param item
	 * @return true para jogo atualizado
	 * @throws UpdateException
	 */
	public boolean update(JogosVideogame item) throws UpdateException {
		if (videogame.contains(item)) {
			System.out.println("----------ATUALIZAR REGISTRO DO VIDEO GAME----------");
			item.setTitulo(in.nextLine());
			item.setEstado(Estado.valueOf(in.nextLine()));
			item.setPreco(in.nextDouble());
			item.setObservacoes(in.next());
			item.setImportante(in.nextBoolean());
			item.setConsole(Console.valueOf(in.next()));
			item.setStatusZerado(in.nextBoolean());
			item.setStatusDlcs(in.nextBoolean());
			videogame.add(item);
			logger.info("O jogo de video game foi atualizado: {}", true);
			return true;
		} else
			throw new UpdateException("O jogo de video game nao esta registrado");
	}

	/**
	 * Metodo de deletar um jogo de video game
	 * 
	 * @param item
	 * @return true para jogo removido
	 * @throws DeleteException
	 */
	public boolean delete(JogosVideogame item) throws DeleteException {
		logger.info("Executando o metodo delete", item);
		if (videogame.contains(item)) {
			videogame.remove(item);
			logger.info("O jogo de video game foi removido: {}", true);
			return true;
		} else
			throw new DeleteException("O jogo de video game nao esta registrado");
	}

	/**
	 * Metodo de listar video game
	 * 
	 * @return List
	 */
	public List<JogosVideogame> listar() {
		return videogame;
	}

	/**
	 * Main para teste de criate, update e delete
	 * 
	 * @param args
	 */
	public static void main(String[] args) {

		JogosVideogameDAOWithLogs jogVG = new JogosVideogameDAOWithLogs();

		JogosVideogame item = new JogosVideogame(1, "damas", Estado.NOVO, 100.00, "legal", true, Console.XBOXONE, true,
				true);

		try {

			jogVG.create(item);
			jogVG.create(item);

		} catch (Exception e) {
			logger.error(e.getMessage());
			logger.error("Erro ao inserir jogo de video game", e);
		}

		try {
			jogVG.update(item);

		} catch (Exception a) {
			logger.error(a.getMessage());
			logger.error("Erro ao atualizar jogo de video game ", a);

		}

		try {
			jogVG.delete(item);

		} catch (Exception b) {
			logger.error(b.getMessage());
			logger.error("Erro ao remover jogo de video game ", b);

		}
	}

}
