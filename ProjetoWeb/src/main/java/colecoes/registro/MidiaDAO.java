package colecoes.registro;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import colecoes.enums.Estado;
import colecoes.itens.Midia;
import excecoes.CreateException;
import excecoes.DeleteException;
import excecoes.UpdateException;
import interface_generica.Generics;

/**
 * Classe MidiaDAO que implementa a interface Generics
 * 
 * @author Adriele, Samara e Tamyres
 */
public class MidiaDAO implements Generics<Midia> {

	private List<Midia> midias;

	Scanner in = new Scanner(System.in);

	/**
	 * Construtor
	 */
	public MidiaDAO() {
		midias = new ArrayList<Midia>();
	}

	@Override
	public boolean create(Midia item) throws CreateException {
		if (!midias.contains(item)) {
			midias.add(item);
			return true;
		} else
			throw new CreateException("A midia ja esta registrada");
	}

	@Override
	public boolean update(Midia item) throws UpdateException {
		if (midias.contains(item)) {
			System.out.println("----------ATUALIZAR REGISTRO DA MIDIA----------");
			item.setId(in.nextInt());
			item.setTitulo(in.nextLine());
			item.setEstado(Estado.valueOf(in.nextLine()));
			item.setPreco(in.nextDouble());
			item.setObservacoes(in.nextLine());
			item.setImportante(in.nextBoolean());
			item.setMarca(in.nextLine());
			item.setConteudo(in.nextLine());
			item.setStatusAssistido(in.nextBoolean());
			midias.add(item);
			return true;
		} else
			throw new UpdateException("A midia nao esta registrada");
	}

	@Override
	public boolean delete(Midia item) throws DeleteException {
		if (midias.contains(item)) {
			midias.remove(item);
			return true;
		} else
			throw new DeleteException("A midia nao esta registrada");
	}

	@Override
	public List<Midia> listar() {
		return midias;
	}

}
