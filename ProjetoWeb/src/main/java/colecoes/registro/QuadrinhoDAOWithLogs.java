package colecoes.registro;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import colecoes.enums.Estado;
import colecoes.itens.Midia;
import colecoes.itens.Quadrinho;
import excecoes.CreateException;
import excecoes.DeleteException;
import excecoes.UpdateException;

/**
 * Classe QuadrinhoDAOWithLogs
 * 
 * @author Adriele, Samara, Tamyres
 */
public class QuadrinhoDAOWithLogs {

	private List<Quadrinho> quadrinhos;

	Scanner in = new Scanner(System.in);
	private static Logger logger = LoggerFactory.getLogger(JogosTabuleiroDAOWithLogs.class);

	/**
	 * Construtor
	 */
	public QuadrinhoDAOWithLogs() {
		quadrinhos = new ArrayList<Quadrinho>();
	}

	/**
	 * Metodo de criar quadrinhos
	 * 
	 * @param item
	 * @return true para quadrinho registrado
	 * @throws CreateException
	 */
	public boolean create(Quadrinho item) throws CreateException {
		logger.info("Executando o metodo create", item);
		if (!quadrinhos.contains(item)) {
			quadrinhos.add(item);
			logger.info("O Quadrinho foi registrado: {}", true);
			return true;
		} else
			throw new CreateException("O quadrinho ja foi registrado");
	}

	/**
	 * Metodo de deletar quadrinhos
	 * 
	 * @param item
	 * @return true para quadrinho removido
	 * @throws DeleteException
	 */
	public boolean delete(Quadrinho item) throws DeleteException {
		logger.info("Executando o metodo delete", item);
		if (quadrinhos.contains(item)) {
			quadrinhos.remove(item);
			logger.info("O Quadrinho foi removido: {}", true);
			return true;
		} else
			throw new DeleteException("O quadrinho nao esta registrado");

	}

	/**
	 * Metodo de atualizar quadrinhos
	 * 
	 * @param item
	 * @return true para quadrinho atualizado
	 * @throws UpdateException
	 */
	public boolean update(Quadrinho item) throws UpdateException {
		logger.info("Executando o metodo update", item);
		if (quadrinhos.contains(item)) {
			System.out.println("----------ATUALIZAR REGISTRO DO QUADRINHO----------");
			item.setTitulo(in.nextLine());
			item.setEstado(Estado.valueOf(in.next()));
			item.setPreco(in.nextDouble());
			item.setObservacoes(in.next());
			item.setImportante(in.nextBoolean());
			item.setNumero(in.nextInt());
			item.setEditora(in.next());
			item.setUniverso(in.next());
			item.setSaga(in.next());
			item.setStatusLido(in.nextBoolean());
			quadrinhos.add(item);
			logger.info("O Quadrinho foi atualizado: {}", true);
			return true;
		} else
			throw new UpdateException("O quadrinho nao esta registrado");
	}

	/**
	 * Metodo de listar quadrinhos
	 * 
	 * @return List
	 */
	public List<Quadrinho> listar() {
		return quadrinhos;
	}

	/**
	 * Main para teste de criate, update e delete
	 * 
	 * @param args
	 */
	public static void main(String[] args) {

		QuadrinhoDAOWithLogs quadro = new QuadrinhoDAOWithLogs();

		Quadrinho item = new Quadrinho(01, "X-Men", Estado.RASURADO, 50, " ", false, 10, "Panini comics", "DC", " ",
				false);

		try {

			quadro.create(item);
			quadro.create(item);

		} catch (Exception e) {
			logger.error(e.getMessage());
			logger.error("Erro ao inserir quadrinho", e);
		}

		try {
			quadro.update(item);

		} catch (Exception a) {
			logger.error(a.getMessage());
			logger.error("Erro ao atualizar quadrinho ", a);

		}

		try {
			quadro.delete(item);

		} catch (Exception b) {
			logger.error(b.getMessage());
			logger.error("Erro ao remover quadrinho ", b);

		}
	}

}
