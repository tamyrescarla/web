package colecoes.registro;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import colecoes.enums.Console;
import colecoes.enums.Estado;
import colecoes.itens.JogosVideogame;
import colecoes.itens.Midia;
import excecoes.CreateException;
import excecoes.DeleteException;
import excecoes.UpdateException;

/**
 * Classe MidiaDAOWithLogs
 * 
 * @author Adriele, Samara, Tamyres
 */
public class MidiaDAOWithLogs {
	private List<Midia> midias;

	Scanner in = new Scanner(System.in);
	private static Logger logger = LoggerFactory.getLogger(JogosTabuleiroDAOWithLogs.class);

	/**
	 * Construtor
	 */
	public MidiaDAOWithLogs() {
		midias = new ArrayList<Midia>();
	}

	/**
	 * Metodo de criar uma midia
	 * 
	 * @param item
	 * @return true para midia registrada
	 * @throws CreateException
	 */
	public boolean create(Midia item) throws CreateException {
		logger.info("Executando o metodo create", item);
		if (!midias.contains(item)) {
			midias.add(item);
			logger.info("A Midia foi registrada: {}", true);
			return true;
		} else
			throw new CreateException("A midia ja esta registrada");
	}

	/**
	 * Metodo de atualizar uma midia
	 * 
	 * @param item
	 * @return true para midia atualizada
	 * @throws UpdateException
	 */
	public boolean update(Midia item) throws UpdateException {
		logger.info("Executando o metodo update", item);
		if (midias.contains(item)) {
			System.out.println("----------ATUALIZAR REGISTRO DA MIDIA----------");

			item.setTitulo(in.nextLine());
			item.setEstado(Estado.valueOf(in.next()));
			item.setPreco(in.nextDouble());
			item.setObservacoes(in.next());
			item.setImportante(in.nextBoolean());
			item.setMarca(in.next());
			item.setConteudo(in.next());
			item.setStatusAssistido(in.nextBoolean());
			midias.add(item);
			logger.info("A Midia foi atualizada: {}", true);
			return true;
		} else
			throw new UpdateException("A midia nao esta registrada");
	}

	/**
	 * Metodo de deletar uma midia
	 * 
	 * @param item
	 * @return true para midia removida
	 * @throws DeleteException
	 */
	public boolean delete(Midia item) throws DeleteException {
		logger.info("Executando o metodo delete", item);
		if (midias.contains(item)) {
			midias.remove(item);
			logger.info("A Midia foi removida: {}", true);
			return true;
		} else
			throw new DeleteException("A midia nao esta registrada");
	}

	/**
	 * Metodo de listar midias
	 * 
	 * @return List
	 */
	public List<Midia> listar() {
		return midias;
	}

	/**
	 * Main para teste de criate, update e delete
	 * 
	 * @param args
	 */
	public static void main(String[] args) {

		MidiaDAOWithLogs midia = new MidiaDAOWithLogs();

		Midia item = new Midia(1, "damas", Estado.NOVO, 100.00, "legal", true, "aaaa", "terror", true);

		try {

			midia.create(item);
			midia.create(item);

		} catch (Exception e) {
			logger.error(e.getMessage());
			logger.error("Erro ao inserir midia", e);
		}

		try {
			midia.update(item);

		} catch (Exception a) {
			logger.error(a.getMessage());
			logger.error("Erro ao atualizar midia ", a);

		}

		try {
			midia.delete(item);

		} catch (Exception b) {
			logger.error(b.getMessage());
			logger.error("Erro ao remover midia ", b);

		}
	}

}
