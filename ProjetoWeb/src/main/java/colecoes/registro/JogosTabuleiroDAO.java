package colecoes.registro;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import colecoes.enums.Estado;
import colecoes.itens.JogosTabuleiro;
import excecoes.CreateException;
import excecoes.DeleteException;
import excecoes.UpdateException;
import interface_generica.Generics;

/**
 * Classe JogosTabuleioDAO que implementa a interface Generics
 * @author Adriele, Samara e Tamyres
 */
public class JogosTabuleiroDAO implements Generics<JogosTabuleiro> {
	
	private List<JogosTabuleiro> tabuleiro;
	
	Scanner in = new Scanner(System.in);

	/**
	 * Construtor 
	 */
	public JogosTabuleiroDAO() {
		tabuleiro = new ArrayList<JogosTabuleiro>();
	}

	@Override
	public boolean create(JogosTabuleiro item) throws CreateException {
		if (!tabuleiro.contains(item)) {
			tabuleiro.add(item);
			return true;
		} else throw new CreateException("O jogo de tabuleiro ja esta registrado");
	}

	@Override
	public boolean update(JogosTabuleiro item) throws UpdateException {
		if (tabuleiro.contains(item)) {
			System.out.println("----------ATUALIZAR REGISTRO DO JOGO DE TABULEIRO----------");
			item.setId(in.nextInt());
			item.setTitulo(in.nextLine());
			item.setEstado(Estado.valueOf(in.nextLine()));
			item.setPreco(in.nextDouble());
			item.setObservacoes(in.nextLine());
			item.setImportante(in.nextBoolean());
			tabuleiro.add(item);
			return true;
		} else throw new UpdateException("O jogo de tabuleiro nao esta registrado");
	}

	@Override
	public boolean delete(JogosTabuleiro item) throws DeleteException {
		if (tabuleiro.contains(item)) {
			tabuleiro.remove(item);
			return true;
		} else throw new DeleteException("O jogo de tabuleiro nao esta registrado");
	}

		
	@Override
	public List<JogosTabuleiro> listar() {
			return tabuleiro;
	}
}
