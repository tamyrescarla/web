package colecoes.registro;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import colecoes.enums.Estado;
import colecoes.itens.Quadrinho;
import excecoes.CreateException;
import excecoes.DeleteException;
import excecoes.UpdateException;
import interface_generica.Generics;

/**
 * Classe QuadrinhoDAO que implementa a interface Generics
 * 
 * @author Adriele, Samara e Tamyres
 */
public class QuadrinhoDAO implements Generics<Quadrinho> {

	private List<Quadrinho> quadrinhos;

	Scanner in = new Scanner(System.in);

	/**
	 * Construtor
	 */
	public QuadrinhoDAO() {
		quadrinhos = new ArrayList<Quadrinho>();
	}

	@Override
	public boolean create(Quadrinho item) throws CreateException {
		if (!quadrinhos.contains(item)) {
			quadrinhos.add(item);
			return true;
		} else
			throw new CreateException("O quadrinho ja foi registrado");
	}

	@Override
	public boolean delete(Quadrinho item) throws DeleteException {
		if (quadrinhos.contains(item)) {
			quadrinhos.remove(item);
			return true;
		} else
			throw new DeleteException("O quadrinho nao esta registrado");

	}

	@Override
	public boolean update(Quadrinho item) throws UpdateException {
		if (quadrinhos.contains(item)) {
			System.out.println("----------ATUALIZAR REGISTRO DO QUADRINHO----------");
			item.setId(in.nextInt());
			item.setTitulo(in.nextLine());
			item.setEstado(Estado.valueOf(in.nextLine()));
			item.setPreco(in.nextDouble());
			item.setObservacoes(in.nextLine());
			item.setImportante(in.nextBoolean());
			item.setNumero(in.nextInt());
			item.setEditora(in.nextLine());
			item.setUniverso(in.nextLine());
			item.setSaga(in.nextLine());
			item.setStatusLido(in.nextBoolean());
			quadrinhos.add(item);
			return true;
		} else
			throw new UpdateException("O quadrinho nao esta registrado");
	}

	@Override
	public List<Quadrinho> listar() {
		return quadrinhos;
	}

}
