package interface_generica;

import java.util.List;

import excecoes.CreateException;
import excecoes.DeleteException;
import excecoes.UpdateException;

/**
 * Interface Generics que possui as assinaturas dos metodos para a implementacao do CRUD
 * @author Adriele, Samara e Tamyres
 * @param <T>
 */
public interface Generics<T> {
	
	/**
	 * Assinatura do metodo para criar um novo registro
	 * @param item
	 * @throws CreateException 
	 * @throws SQLException 
	 */
	public boolean create(T item) throws CreateException;
	
	/**
	 * Assinatura do metodo para editar um registro
	 * @param item
	 */
	public boolean update(T item) throws UpdateException;
	
	/**
	 * Assinatura do metodo para excluir um registro
	 * @param item
	 * @throws DeleteException 
	 */
	public boolean delete(T item) throws DeleteException;
	
	/**
	 * Assinatura do metodo para retornar uma lista de itens
	 * @return
	 * @throws SQLException 
	 */
	public List<T> listar();

}
