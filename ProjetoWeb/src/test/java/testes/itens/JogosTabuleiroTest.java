package testes.itens;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import colecoes.enums.Estado;
import colecoes.itens.JogosTabuleiro;
import colecoes.registro.JogosTabuleiroDAO;
import excecoes.CreateException;
import excecoes.DeleteException;

/**
 * Classe JogosTabuleiroTest
 * @author Adriele, Samara, Tamyres
 */
public class JogosTabuleiroTest {
	
	JogosTabuleiro tabuleiro;
	JogosTabuleiroDAO dao;

	@Before
	public void setUp() throws Exception {
		tabuleiro = new JogosTabuleiro(01, "Ludo", Estado.NOVO, 30, "De dois a quatro jogadores", false);
		dao = new JogosTabuleiroDAO();
	}

	/**
	 * Cria registro
	 * @throws CreateException
	 */
	@Test
	public void testCriarRegistro() throws CreateException {
		assertTrue(dao.create(tabuleiro));
	}
	
	/**
	 * Verifica os dados
	 */
	@Test
	public void testDados() {
		assertEquals(01, tabuleiro.getId());
		assertEquals("Ludo", tabuleiro.getTitulo());
		assertEquals(Estado.NOVO, tabuleiro.getEstado());
		assertEquals(30, tabuleiro.getPreco(), 0);
		assertEquals("De dois a quatro jogadores", tabuleiro.getObservacoes());
		assertEquals(false, tabuleiro.getImportante());
	}
	
	/**
	 * Cria um registro ja cadastrado
	 * @throws CreateException
	 */
	@Test(expected = CreateException.class)
	public void testCriarRegistroExistente() throws CreateException {
		dao.create(tabuleiro);
		assertEquals("O jogo de tabuleiro ja esta registrado", dao.create(tabuleiro));
	}
	
	/**
	 * Exclui um registro
	 * @throws DeleteException
	 * @throws CreateException
	 */
	@Test
	public void testExcluirRegistro() throws DeleteException, CreateException {
		dao.create(tabuleiro);
		assertTrue(dao.delete(tabuleiro));
	}
	
	/**
	 * Exclui um registro que nao esta cadastrado
	 * @throws DeleteException
	 */
	@Test(expected = DeleteException.class)
	public void testExcluirRegistroInexistente() throws DeleteException {
		assertEquals("O jogo de tabuleiro nao esta registrado", dao.delete(tabuleiro));
	}


}
