package testes.itens;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import colecoes.enums.Console;
import colecoes.enums.Estado;
import colecoes.itens.JogosVideogame;
import colecoes.registro.JogosVideogameDAO;
import excecoes.CreateException;
import excecoes.DeleteException;

/**
 * Classe JogosVideogameTest
 * @author Adriele, Samara, Tamyres
 */
public class JogosVideogameTest {
	
	JogosVideogame videogame;
	JogosVideogameDAO dao;

	@Before
	public void setUp() throws Exception {
		videogame = new JogosVideogame(01, "Mario Bros", Estado.NOVO, 100, " ", false, Console.NITENDO, false, false);
		dao = new JogosVideogameDAO();
	}
	
	/**
	 * Verifica os dados
	 */
	@Test
	public void testDados() {
		assertEquals(01, videogame.getId());
		assertEquals("Mario Bros", videogame.getTitulo());
		assertEquals(Estado.NOVO, videogame.getEstado());
		assertEquals(100, videogame.getPreco(), 0);
		assertEquals(" ", videogame.getObservacoes());
		assertEquals(false, videogame.getImportante());
		assertEquals(Console.NITENDO, videogame.getConsole());
		assertEquals(false, videogame.getStatusZerado());
		assertEquals(false, videogame.getStatusDlcs());
	}

	/**
	 * Cria registro
	 * @throws CreateException
	 */
	@Test
	public void testCriarRegistro() throws CreateException {
		assertTrue(dao.create(videogame));
	}
	
	/**
	 * Cria um registro ja cadastrado
	 * @throws CreateException
	 */
	@Test(expected = CreateException.class)
	public void testCriarRegistroExistente() throws CreateException {
		dao.create(videogame);
		assertEquals("O jogo de video game ja esta registrado", dao.create(videogame));
	}
	
	/**
	 * Exclui um registro
	 * @throws DeleteException
	 * @throws CreateException
	 */
	@Test
	public void testExcluirRegistro() throws DeleteException, CreateException {
		dao.create(videogame);
		assertTrue(dao.delete(videogame));
	}
	
	/**
	 * Exclui um registro que n�o est� cadastrado
	 * @throws DeleteException
	 */
	@Test(expected = DeleteException.class)
	public void testExcluirRegistroInexistente() throws DeleteException {
		assertEquals("O jogo de video game nao esta registrado", dao.delete(videogame));
	}


}
