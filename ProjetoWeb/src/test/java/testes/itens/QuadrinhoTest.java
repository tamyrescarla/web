package testes.itens;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import colecoes.enums.Estado;
import colecoes.itens.Quadrinho;
import colecoes.registro.QuadrinhoDAO;
import excecoes.CreateException;
import excecoes.DeleteException;

/**
 * Classe QuadrinhoTest 
 * @author Adriele, Samara, Tamyres
 */
public class QuadrinhoTest {

	Quadrinho hq;
	QuadrinhoDAO dao;
	
	@Before
	public void setUp() throws Exception {
		hq = new Quadrinho(01, "X-Men", Estado.RASURADO, 50, " ", false, 10, "Panini comics", "DC", " ", false);
		dao = new QuadrinhoDAO();
	}

	/**
	 * Verifica os dados
	 */
	@SuppressWarnings("deprecation")
	@Test
	public void testDados() {
		assertEquals(01, hq.getId());
		assertEquals("X-Men", hq.getTitulo());
		assertEquals(Estado.RASURADO, hq.getEstado());
		assertEquals(50, hq.getPreco(), 0.5);
		assertEquals(" ", hq.getObservacoes());
		assertFalse(hq.getImportante());
		assertEquals(10, hq.getNumero());
		assertEquals("Panini comics", hq.getEditora());
		assertEquals("DC", hq.getUniverso());
		assertEquals(" ", hq.getSaga());
		assertFalse(hq.getStatusLido());
	}
	
	/**
	 * Criar registro
	 * @throws CreateException
	 */
	@Test
	public void testCriarRegistro() throws CreateException {
		assertTrue(dao.create(hq));
	}
	
	/**
	 * Criar registro ja cadastrado
	 * @throws CreateException
	 */
	@Test(expected = CreateException.class)
	public void testCriarRegistroExistente() throws CreateException {
		dao.create(hq);
		assertEquals("O quadrinho ja foi registrado", dao.create(hq));
	} 
	
	/**
	 * Excluir registro
	 * @throws DeleteException
	 * @throws CreateException
	 */
	@Test 
	public void testExcluirRegistro() throws DeleteException, CreateException {
		dao.create(hq);
		assertTrue(dao.delete(hq));
	}
	
	/**
	 * Excluir registro inexistente
	 * @throws DeleteException
	 * @throws CreateException
	 */
	@Test(expected = DeleteException.class)
	public void testExcluirRegistroInexistente() throws DeleteException, CreateException {
			assertEquals("O quadrinho nao esta registrado",  dao.delete(hq));
	} 
	
}
