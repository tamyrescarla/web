package testes.itens;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import colecoes.enums.Estado;
import colecoes.itens.Midia;
import colecoes.registro.MidiaDAO;
import excecoes.CreateException;
import excecoes.DeleteException;

/**
 * Classe MidiaTest
 * @author Adriele, Samara, Tamyres
 */
public class MidiaTest {
	
	Midia dvd;
	MidiaDAO dao;

	@Before
	public void setUp() throws Exception {
		dvd = new Midia(01, "Calcinha Preta", Estado.VELHO, 40, "E pra se apaixonar", false, "Globo News", "14 musicas", false);
		dao = new MidiaDAO();
	}
	
	/**
	 * Verifica os dados
	 */
	@Test
	public void testDados() {
		assertEquals(01, dvd.getId());
		assertEquals("Calcinha Preta", dvd.getTitulo());
		assertEquals(Estado.VELHO, dvd.getEstado());
		assertEquals("E pra se apaixonar", dvd.getObservacoes());
		assertFalse(dvd.getImportante());
		assertEquals("Globo News", dvd.getMarca());
		assertEquals("14 musicas", dvd.getConteudo());
		assertFalse(dvd.getStatusAssistido());
	}

	/**
	 * Cria registro
	 * @throws CreateException
	 */
	@Test
	public void testCriarRegistro() throws CreateException {
		assertTrue(dao.create(dvd));
	}
	
	/**
	 * Cria um registro ja cadastrado
	 * @throws CreateException
	 */
	@Test(expected = CreateException.class)
	public void testCriarRegistroExistente() throws CreateException {
		dao.create(dvd);
		assertEquals("A midia ja esta registrada", dao.create(dvd));
	}
	
	/**
	 * Exclui um registro
	 * @throws DeleteException
	 * @throws CreateException
	 */
	@Test
	public void testExcluirRegistro() throws DeleteException, CreateException {
		dao.create(dvd);
		assertTrue(dao.delete(dvd));
	}
	
	/**
	 * Exclui um registro que nao esta cadastrado
	 * @throws DeleteException
	 */
	@Test(expected = DeleteException.class)
	public void testExcluirRegistroInexistente() throws DeleteException {
		assertEquals("A midia nao esta registrada", dao.delete(dvd));
	}


}
