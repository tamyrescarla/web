package testes.usuario;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import excecoes.CreateException;
import excecoes.DeleteException;
import usuario.Usuario;
import usuario.registro.UsuarioDAO;

/**
 * Classe UsuarioTest
 * @author Adriele, Samara, Tamyres
 */
public class UsuarioTest {
	
	Usuario usuario;
	UsuarioDAO dao;

	@Before
	public void setUp() throws Exception {
		usuario = new Usuario(1, "samaslima", "1234", "Samara Lima");
		dao = new UsuarioDAO();
	}
	
	/**
	 * Verifica os dados
	 */
	@Test
	public void testDados() {
		assertEquals(1, usuario.getId());
		assertEquals("samaslima", usuario.getLogin());
		assertEquals("1234", usuario.getSenha());
		assertEquals("Samara Lima", usuario.getNome());
	}

	/**
	 * Cria registro
	 * @throws CreateException
	 */
	@Test
	public void testCriarRegistro() throws CreateException {
		assertTrue(dao.create(usuario));
	}
	
	/**
	 * Cria um registro ja cadastrado
	 * @throws CreateException
	 */
	@Test(expected = CreateException.class)
	public void testCriarRegistroExistente() throws CreateException {
		dao.create(usuario);
		assertEquals("O usuario ja esta registrado", dao.create(usuario));
	}
	
	/**
	 * Exclui um registro
	 * @throws DeleteException
	 * @throws CreateException
	 */
	@Test
	public void testExcluirRegistro() throws DeleteException, CreateException {
		dao.create(usuario);
		assertTrue(dao.delete(usuario));
	}
	
	/**
	 * Exclui um registro que nao esta cadastrado
	 * @throws DeleteException
	 */
	@Test(expected = DeleteException.class)
	public void testExcluirRegistroInexistente() throws DeleteException {
		assertEquals("O usuario nao esta registrado", dao.delete(usuario));
	}

}
