package testes.usuario;

import static org.junit.Assert.*;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import excecoes.CreateException;
import excecoes.DeleteException;
import usuario.Amigo;
import usuario.registro.AmigoDAO;

/**
 * Classe AmigoTest
 * @author Adriele, Samara, Tamyres
 */
public class AmigoTest {
	
	Amigo amigo;
	AmigoDAO dao;

	@Before
	public void setUp() throws Exception {
		amigo = new Amigo(4, "Carlos", "Masculino", "Primo", "Rua Sebastiao Nicolau, Nº 149", "(83)99999-9999");
		dao = new AmigoDAO();
	}
	
	/**
	 * Verifica os dados
	 */
	@Test
	public void testDados() {
		assertEquals(4, amigo.getIdAmigo());
		assertEquals("Carlos", amigo.getNome());
		assertEquals("Masculino", amigo.getSexo());
		assertEquals("Primo", amigo.getParentesco());
		assertEquals("Rua Sebastiao Nicolau, Nº 149", amigo.getEndereco());
		assertEquals("(83)99999-9999", amigo.getTelefone());
	}

	/**
	 * Cria registro
	 * @throws CreateException
	 */
	@Test
	public void testCriarRegistro() throws CreateException {
		assertTrue(dao.create(amigo));
	}
	
	/**
	 * Cria um registro ja cadastrado
	 * @throws CreateException
	 */
	@Test(expected = CreateException.class)
	public void testCriarRegistroExistente() throws CreateException {
		dao.create(amigo);
		assertEquals(dao.create(amigo), false);
	}
	
	/**
	 * Exclui um registro
	 * @throws DeleteException
	 * @throws CreateException
	 */
	@Test
	public void testExcluirRegistro() throws DeleteException, CreateException {
		dao.create(amigo);
		assertTrue(dao.delete(amigo));
	}
	
	/**
	 * Exclui um registro que nao esta cadastrado
	 * @throws DeleteException
	 */
	@Test(expected = DeleteException.class)
	public void testExcluirRegistroInexistente() throws DeleteException {
		assertEquals("O amigo nao esta registrado", "O amigo nao esta registrado", dao.delete(amigo));
	}

}
